#Import from the core django
from django import forms
#Import from local app/library
from django.contrib.auth.models import User


class LoginForm(forms.ModelForm):
      class Meta:
        model= User
        fields=  ['username' ,'password'  ]  

class RegisterForm(forms.ModelForm):
      class Meta:
        model= User
        fields=  ['id', 'username', 'email', 'password'  ]          