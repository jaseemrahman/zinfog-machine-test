#Import from the core django
from django.shortcuts import render
from django.contrib.auth import authenticate, login, logout
from django.shortcuts import redirect, render
from django.contrib.auth.models import User

#Import from the third library
from rest_framework.decorators import api_view
from rest_framework.response import Response

import requests
#Import from local app/library
from product.models import Customer
from accounts.forms import LoginForm,RegisterForm


def logout_user(request):
    logout(request)
    return redirect('/')

def customer_list(request):
    customers=Customer.objects.all()  
    context={'customers':customers,}   
    return render(request,'accounts/customer_list.html',context) 



def user_login(request):
    if request.method == 'POST':
        print(request.POST)
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(request, username=username, password=password)
        val = requests.post('http://127.0.0.1:8000/accounts/api/login/', data=request.POST)
        if val.status_code == 200:
            login(request, user)
            return redirect("product.list",)
    form = LoginForm()
    context = {'form': form,}
    print(form.errors)
    return render(request,'accounts/login.html',context)    

def user_signup(request):
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        
        val = requests.post('http://127.0.0.1:8000/accounts/api/register/', data=request.POST)
        print(val)
        if val.status_code == 200:
            user = authenticate(request, username=username, password=password)
            login(request, user)
            return redirect("product.list",)
    form = RegisterForm()
    context = {'form': form,}
    print(form.errors)
    return render(request,'accounts/signup.html',context)        


    
