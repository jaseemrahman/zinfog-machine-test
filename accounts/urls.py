#Import from the core django
from django.urls import path
#Import from local app/library
from accounts import views
from .api_views import RegisterAPI,LoginAPI
urlpatterns = [
    #home page
    path('customer_list',views.customer_list,name='customer.list'),
    path('logout',views.logout_user,name='logout'),
    #login
    path('login', views.user_login, name='login'),
    #signup
    path('signup', views.user_signup, name='signup'),
    #api
    path('api/register/', RegisterAPI.as_view(), name='api.register'),
    path('api/login/', LoginAPI.as_view(), name='api.login'),

]