#Import from the core django
from django import forms
#Import from local app/library
from .models import Product,Order,Customer

#Product
class ProductForm(forms.ModelForm):
    class Meta:
        model= Product
        fields= '__all__'

#Order
class OrderForm(forms.ModelForm):
      class Meta:
        model= Customer
        fields= '__all__'
        # fields=  ['address' , 'mail_id' ]     

class StatusForm(forms.ModelForm):
      class Meta:
        model=Order
        # fields= '__all__'
        fields=  ['order_status']       
        
          