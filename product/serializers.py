from dataclasses import fields
from pyexpat import model
from rest_framework import serializers
from product.models import Product,Order,OrderDetail

class Productserializers(serializers.ModelSerializer):
    class Meta:
        model=Product
        fields='__all__'

class Orderserializers(serializers.ModelSerializer):
    class Meta:
        model=Order
        fields='__all__'        