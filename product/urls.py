#Import from the core django
from django.urls import path
#Import from local app/library
from product import views

urlpatterns = [
    #home page
    path('',views.product_list,name='product.list'),
    path('create',views.product_create,name='product.create'),
    path('update/<int:pk>',views.product_update,name='product.update'),
    path('delete/<int:pk>',views.product_delete,name='product.delete'),
    path('view/<int:pk>',views.product_view,name='product.view'),

    #cart
    path('cart',views.viewcart,name='cart'),
    path('addtocart',views.addtocart,name='add.to.cart'),
    path('removefromcart',views.cart_remove,name='remove.from.cart'),
    path('cart/order',views.order_create,name='cart.order'),

    #api
    path('api/get',views.product_list_api),
    
    # order list
    path('api/order_list/<int:pk>',views.order_list_api),
    path('order_list/<int:pk>',views.order_list,name='order.list'),

    #order detail
    path('order_list/details/<int:pk>',views.order_detail_view,name='order.detail'),

    #rating
    path('rating/<int:pk>',views.rating,name='rating'),

    #order status
    path('order_status/<int:pk>',views.order_status,name='order.status'),

]