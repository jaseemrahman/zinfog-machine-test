#Import from the core django
from django.shortcuts import render
from django.shortcuts import redirect
from django.template.loader import render_to_string
from django.http import JsonResponse,HttpResponse
from django.template import loader
from django.contrib import messages
from django.template.loader import get_template
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.conf import settings
from django.core.mail import send_mail
from django.http import HttpResponseRedirect
from django.urls import reverse
#Import from the third library
from rest_framework.decorators import api_view
from rest_framework.response import Response
import requests
from datetime import datetime

#Import from local app/library
from product.forms import ProductForm ,OrderForm,StatusForm
from product.models import Product,OrderDetail,Order,Customer
from product.serializers import Productserializers,Orderserializers 



# Create your views here.


@api_view(['GET'])
def product_list_api(request):
    if request.method=='GET':
        val=Product.objects.all()
        print(val)
        serializer=Productserializers(val, many=True)
        return Response(serializer.data)



#View for Product list
def product_list(request):
    response = requests.get('http://127.0.0.1:8000/api/get')
    products = response.json()
    context= {'products':products,"today":datetime.today(),}
    return render(request, 'product/product_list.html',context)

@api_view(['GET'])
def order_list_api(request,pk):
    if request.method=='GET':
        
        # customer=Customer.objects.get(pk=pk)
        # print(customer)
        # orders=Order.objects.filter(customer=customer)
        # print(orders)
        # val=[]
        # for customer_order in customer_orders:
        #     order_details=OrderDetail.objects.filter(order_id=customer_order.id)
        #     val.append(order_details)
        # print(val)   
        user=User.objects.get(pk=pk) 
        # user=self.request.user
        # print(user)
        orders=Order.objects.filter(user=user)
        # print(orders)
        serializer=Orderserializers(orders,many=True)
        return Response(serializer.data)

#View for Product list
def order_list(request,pk):
    # print(request)
    response = requests.get('http://127.0.0.1:8000/api/order_list/'+str(pk))
    # print(response)
    orders = response.json()
    # print(orders)
    context= {'orders':orders}
    return render(request, 'product/order_list.html',context)    




#View for Create Product
@login_required
def product_create(request):
    if request.method == 'POST':
        form = ProductForm(request.POST,files=request.FILES)
        if form.is_valid():
            form.save()
            return redirect("product.list")        
    else:
        form = ProductForm()

    context = {'form': form}
    return render(request,'product/product_edit.html',context) 

#View for edit product 
@login_required
def product_update(request,pk):
    product=Product.objects.get(id=pk)
    if request.method == 'POST':
        form = ProductForm(request.POST,files=request.FILES,instance=product)
        if form.is_valid():
            form.save()
            return redirect("product.list",)
    else:
        form = ProductForm(instance=product)
    context = {'form': form,'product': product}
    return render(request,'product/product_edit.html',context) 

#View for product delete
@login_required
def product_delete(request, pk):
    data = dict()
    
    if request.method == 'POST':
        product = Product.objects.get(pk=pk)
        product.delete()
        return redirect('product.list') 
        # data['form_is_valid'] = True
        # products=Product.objects.all() 
        # data['html_product_list'] = render_to_string('product/partial_product_list.html', {
        #         'products': products
        #     })
        
    else:
        product = Product.objects.get(pk=pk)
        context = {'product': product}
        data['html_form'] = render_to_string('product/partial_product_delete.html',
        context,
        request=request,
    )

        return JsonResponse(data) 

#view for product detail view
def product_view(request,pk):
    product=Product.objects.get(pk=pk)    
    context={'product':product}   
    return render(request,'product/product_view.html',context)     

#add to cart view
def addtocart(request):
    productid = request.GET['productid']
    qty = request.GET['qty']
    cartitems = {}
    if request.session.__contains__('cartdata'):
        cartitems = request.session['cartdata']
    cartitems[productid]=qty
    request.session['cartdata'] = cartitems
    current_item=Product.objects.get(pk=productid)

    products=Product.objects.all()     
    context={'products':products,
            "today":datetime.today(),
            } 
    return render(request,'product/product_list.html',context)


#to remove cart item
def cart_remove(request):
    print(request)
    productid = request.GET['productid']
    cartitems = {}
    if request.session.__contains__('cartdata'):
        cartitems = request.session['cartdata']
    request.session['cartdata'] = cartitems
    # print( request.session['cartdata'])
    del cartitems[productid]
    return redirect('cart')    
     
#View for order create
@login_required
def order_create(request):
    data = dict()
    if request.method == 'POST':

        form = OrderForm(request.POST,files=request.FILES)
        if form.is_valid():
            print(form.errors)
            customer=form.save()
            orderd_user=request.user
            order=Order.objects.create(customer=customer,user=orderd_user)
            order_id=Order.objects.get(pk=order.id)
            cartitems = {}
            if request.session.__contains__('cartdata'):
                cartitems = request.session['cartdata']
            request.session['cartdata'] = cartitems
            val=request.session['cartdata']
            for key,value in (zip((val.keys()), (val.values()))):
                product=Product.objects.get(pk=key)
                order_detail=OrderDetail.objects.create(order_id=order_id,product=product,qty=value,total=int(value)*product.price)
           
            subject = f'INVOICE -{customer}'
            with open('message.txt','r') as f:
                f=f.read()
                message=f

            email_from = settings.EMAIL_HOST_USER
            recipient_list = [customer.mail_id]
    

            send_mail( subject, message, email_from, recipient_list )

            cartitems.clear()
            messages.success(request, 'Order Placed Successfully')
            return redirect("product.list")        
    else:
        form = OrderForm()

    context = {'form': form}
    data['html_form'] = render_to_string('product/order.html',context,request=request,)
    return JsonResponse(data)  
       


#cart view
def viewcart(request):
    page = loader.get_template("product/shoppingcart.html") 
    if request.session.__contains__('cartdata'):
        # for key in request.session.keys():
            it = request.session['cartdata'].items()
            productdb =[]
            it = list(it)   
            for i in range(len(it)):
                productid = it[i][0]
                qty = it[i][1]
                db = Product.objects.get(id=productid)
                productdb.append({
                    'id':productid,
                    'title':db.title,
                    'Qty':qty,
                    'Price':db.price,
                    "total_price":int(qty)*db.price
                })
            fullamount = []
            for i in productdb:
                for key,value in i.items():
                    if key == "total_price":
                        fullamount.append(value)
            
            data = {"products":productdb,"full_amount":sum(fullamount)}
            response = page.render(data,request)
            return HttpResponse(response)
    else:
        # return HttpResponse("No item added!!")
        return render(request,'product/shoppingcart.html') 
@login_required
def rating(request,pk):
    print(request.POST)
    product=Product.objects.get(pk=pk)
    rating=e=request.POST['rating']
    if rating :
        product.rating=request.POST['rating']
        product.save()
        return HttpResponseRedirect(reverse('product.view',args=[pk]))
    else:
        return HttpResponseRedirect(reverse('product.view',args=[pk]))


def order_detail_view(request,pk):
    order_details=OrderDetail.objects.filter(order_id=pk)
    # print(order_details)
    order=Order.objects.get(pk=pk)
    form=StatusForm(instance=order)
    context={'order_details':order_details,'pk':pk,'form':form}

    return render(request,'product/order_detail.html',context) 

def order_status(request,pk):
    print(request)
    if request.method== 'POST':
        status=request.POST['order_status']
        order=Order.objects.get(pk=pk)
        order.order_status=status
        order.save()

    return HttpResponseRedirect(reverse('order.detail',args=[pk]))



